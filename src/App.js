import React from 'react';
import {createDrawerNavigator} from 'react-navigation';

import StartStack from './screens/StartScreen';
import PersistenceStack from './screens/PersistenceScreen';
import NetworkingStack from './screens/NetworkingScreen';
import FormStack from './screens/FormScreen';
import MonaStack from './screens/MonaScreen';

export default createDrawerNavigator({
    Start: {
        screen: StartStack,
    },
    Persistence: {
        screen: PersistenceStack,
    },
    Networking: {
        screen: NetworkingStack,
    },
    Form: {
        screen: FormStack,
    },
    Mona: {
        screen: MonaStack,
    },
}, {
    initialRouteName: 'Mona'
});
