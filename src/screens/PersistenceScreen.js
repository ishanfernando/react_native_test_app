import React from 'react';
import {View, Text, Button, Alert, AsyncStorage, ActivityIndicator, StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5';
import db from '../db'

class PersistenceScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        drawerLabel: 'Persistence',
        title: 'Persistence',
        headerLeft: <Icon name='bars' style={styles.drawerOpenIcon} size={30} onPress={() => navigation.openDrawer()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            isBusy: false
        }
    }

    showHideBusyIndicator() {
        if (this.state.isBusy == true) {
            this.setState({isBusy: false});
        } else {
            this.setState({isBusy: true});
        }
    }

    async incrementVal() {
        const val = await AsyncStorage.getItem('val');
        val2 = 1;
        if (val) {
            val2 = val2 + JSON.parse(val);
        }
        await AsyncStorage.setItem('val', JSON.stringify(val2));
    }

    async showVal() {
        const val = await AsyncStorage.getItem('val');
        Alert.alert('AsyncStorage', 'Current value is ' + JSON.parse(val));
    }

    async doInsertRecords() {
        var start = Date.now();

        db.deleteAllItems();
        var secs = (Date.now() - start) / 1000;
        console.log('Deleted all items in ' + secs + ' s');

        var items = [];
        for (var i = 1; i <= 200001; i++) {
            items.push({
                itemNumber: i.toString(),
                name: i.toString(),
                quantity: 1,
                price: 1.5,
                active: true
            });
            if (items.length == 5000) {
                db.insertItems(items);
                items = [];
                console.log('Inserted 5000 items');
            }
        }
        if (items.length > 0) {
            db.insertItems(items);
            console.log('Inserted ' + items.length + ' items');
        }

        secs = (Date.now() - start) / 1000;
        console.log('Inserted 200001 items in ' + secs + ' s');
        this.showHideBusyIndicator();
        Alert.alert('Realm storage', 'Inserted 200001 items in ' + secs + ' s');
    }

    async insertRecords() {
        this.showHideBusyIndicator();
        // give some time to paint busy indicator
        // FIXME is there a better way?
        setTimeout(() => {
            this.doInsertRecords();
        }, 100);
    }

    async readRecords() {
        var count = await db.countItems();
        Alert.alert('Realm storage', 'There are ' + count + ' items');
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text>Persistence Screen</Text>
                <Text style={styles.topicHeader}>AsyncStorage test</Text>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.incrementVal()}
                        title="INCREMENT VALUE"/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.showVal()}
                        title="SHOW VALUE"/>
                </View>
                <Text style={styles.topicHeader}>Realm storage test</Text>
                <View style={styles.buttonContainerMultiComps}>
                    <Button
                        onPress={() => this.insertRecords()}
                        title="INSERT RECORDS"/>
                    {this.state.isBusy ? <ActivityIndicator style={{padding: 5}}/> : null}
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.readRecords()}
                        title="READ RECORDS"/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerOpenIcon: {
        marginLeft: 10
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    topicHeader: {
        marginTop: 40
    },
    buttonContainer: {
        marginTop: 5
    },
    buttonContainerMultiComps: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5
    }
});

export default createStackNavigator({
    Home: {
        screen: PersistenceScreen
    },
});
