import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    Switch,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView
} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5';

class MonaScreen extends React.Component {

    static navigationOptions = ({navigation}) => ({
        drawerLabel: 'Mona',
        title: 'Mona',
        headerLeft: <Icon name='bars' style={styles.drawerOpenIcon} size={30} onPress={() => navigation.openDrawer()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            itemNumber: '0000000',
            customerSearch: false,
            quantity: '0'
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding" keyboardVerticalOffset={64} enabled>
                <ScrollView style={styles.container}>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Modus:</Text>
                        <Text style={styles.fieldValue}>2 - Auftrag</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>KdNr.:</Text>
                        <Text style={styles.fieldValue}>00094386</Text>
                        <Text style={styles.fieldLabel}>Ref.-Nr.:</Text>
                        <Text style={styles.fieldValue}>abcde</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>EAN/ArtNr.:</Text>
                        <TextInput style={[styles.fieldValue, styles.textInput]}
                                   onChangeText={(text) => this.setState({itemNumber: text})}
                                   value={this.state.itemNumber}/>
                        <View style={styles.fieldValue2}>
                            <Text>{this.state.customerSearch ? 'Kunde' : 'Hama'}</Text>
                            <Switch onValueChange={(val) => this.setState({customerSearch: val})}
                                    value={this.state.customerSearch}/>
                        </View>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Artikel:</Text>
                        <Text style={styles.fieldValue}>Artikelname</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Anzahl:</Text>
                        <TextInput style={[styles.fieldValue, styles.textInput]}
                                   onChangeText={(text) => this.setState({quantity: text})}
                                   value={this.state.quantity}
                                   keyboardType='numeric'/>
                        <View style={styles.fieldValue2}>
                            <Icon name='lightbulb' size={30}/>
                        </View>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Bestand:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>Min.Best.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>VPE:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>M.-Kart.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Status:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>FrsatzArt.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Preis:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>KdArtNr.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>So-Preis:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>Rückst.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>UPE:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                        <Text style={styles.fieldLabel}>POS Best.:</Text>
                        <Text style={styles.fieldValue}>0</Text>
                    </View>
                    <View style={styles.fieldRow}>
                        <Text style={styles.fieldLabel}>Artikelbez:</Text>
                        <Text style={styles.fieldValue}>Artikelbeschreibung</Text>
                    </View>
                </ScrollView>
                <View style={styles.buttonRow}>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        alert("TODO")
                    }}>
                        <Icon name='trash' size={30} style={styles.buttonIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        alert("TODO")
                    }}>
                        <Icon name='euro-sign' size={30} style={styles.buttonIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        alert("TODO")
                    }}>
                        <Icon name='store' size={30} style={styles.buttonIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        alert("TODO")
                    }}>
                        <Icon name='retweet' size={30} style={styles.buttonIcon}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => {
                        alert("TODO")
                    }}>
                        <Icon name='save' size={30} style={styles.buttonIcon}/>
                    </TouchableOpacity>
                </View>

            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    drawerOpenIcon: {
        marginLeft: 10
    },
    container: {
        flex: 1,
        backgroundColor: '#ECEFF1'
    },
    fieldRow: {
        alignSelf: 'stretch',
        flexDirection: 'row'
    },
    fieldLabel: {
        marginHorizontal: 5,
        marginVertical: 4,
        width: 75,
        color: 'gray',
        alignSelf: 'flex-end'
    },
    fieldValue: {
        marginHorizontal: 5,
        marginVertical: 4,
        flex: 1
    },
    textInput: {
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        fontSize: 18,
        alignSelf: 'flex-end'
    },
    fieldValue2: {
        marginHorizontal: 5,
        marginVertical: 4,
        width: 60,
        alignItems: 'center'
    },
    buttonRow: {
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 5
    },
    button: {
        flex: 1,
        paddingVertical: 12,
        marginHorizontal: 5,
        alignItems: 'center',
        backgroundColor: '#FF9800',
        borderRadius: 2
    },
    buttonIcon: {
        color: '#FFFFFF'
    }
});

export default createStackNavigator({
    Home: {
        screen: MonaScreen
    },
});
