import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5';

class StartScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        drawerLabel: 'Start',
        title: 'Start',
        headerLeft: <Icon name='bars' style={styles.drawerOpenIcon} size={30} onPress={() => navigation.openDrawer()}/>
    });

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text>Start Screen</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerOpenIcon: {
        marginLeft: 10
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default createStackNavigator({
    Home: {
        screen: StartScreen
    },
});
