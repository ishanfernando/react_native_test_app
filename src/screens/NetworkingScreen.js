import React from 'react';
import {View, Text, Button, Alert, StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5';
import RNFetchBlob from 'rn-fetch-blob';

class NetworkingScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        drawerLabel: 'Networking',
        title: 'Networking',
        headerLeft: <Icon name='bars' style={styles.drawerOpenIcon} size={30} onPress={() => navigation.openDrawer()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            timeFilePath: ''
        }
    }

    async fetchTimeToFile() {
        RNFetchBlob
            .config({
                fileCache: true,
            })
            .fetch('GET', 'http://echo.jsontest.com/time/' + Date.now())
            .then((res) => {
                this.setState({timeFilePath: res.path()});
                Alert.alert('Fetch file test', 'Time saved to\n' + res.path());
            });
    }

    async readTimeFromFile() {
        if (this.state.timeFilePath) {
            RNFetchBlob.fs.readFile(this.state.timeFilePath, 'utf8')
                .then((data) => {
                    Alert.alert('Fetch file test', 'Time in file is\n'
                        + new Date(Number(JSON.parse(data).time)).toString());
                });
        } else {
            Alert.alert('Fetch file test', 'No file found!');
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text>Networking Screen</Text>
                <Text style={styles.topicHeader}>Fetch file test</Text>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.fetchTimeToFile()}
                        title="FETCH CURRENT TIME TO FILE"/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.readTimeFromFile()}
                        title="READ CURRENT TIME FROM FILE"/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerOpenIcon: {
        marginLeft: 10
    },
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    topicHeader: {
        marginTop: 40
    },
    buttonContainer: {
        marginTop: 5
    }
});

export default createStackNavigator({
    Home: {
        screen: NetworkingScreen
    },
});
