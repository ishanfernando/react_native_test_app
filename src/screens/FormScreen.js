import React from 'react';
import {
    View, Text, TextInput, Button, Alert, Switch, Picker, Platform,
    TouchableHighlight, ActionSheetIOS, StyleSheet
} from 'react-native';
import {createStackNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome5';

class FormScreen extends React.Component {
    static navigationOptions = ({navigation}) => ({
        drawerLabel: 'Form',
        title: 'Form',
        headerLeft: <Icon name='bars' style={styles.drawerOpenIcon} size={30} onPress={() => navigation.openDrawer()}/>
    });

    constructor(props) {
        super(props);
        this.state = {
            textInput: 'This is a text input',
            switchValue: true,
            pickerValue: 'Value 2'
        }
    }

    showValues() {
        Alert.alert('Form values', JSON.stringify(this.state));
    }

    showActionSheet() {
        ActionSheetIOS.showActionSheetWithOptions({
                options: ['Value 1', 'Value 2', 'Value 3', 'Cancel'],
                cancelButtonIndex: 3
            },
            (buttonIndex) => {
                if (buttonIndex < 3) {
                    this.setState({pickerValue: 'Value ' + (buttonIndex + 1)});
                }
            });
    }

    createPicker() {
        if (Platform.OS == 'ios') {
            return (
                <TouchableHighlight style={styles.pickerPlaceholder} onPress={() => this.showActionSheet()}>
                    <Text>{this.state.pickerValue}</Text>
                </TouchableHighlight>
            );
        } else if (Platform.OS == 'android') {
            return (
                <Picker
                    selectedValue={this.state.pickerValue}
                    style={styles.picker}
                    mode='dropdown'
                    onValueChange={(itemValue, itemIndex) => this.setState({pickerValue: itemValue})}>
                    <Picker.Item label='Value 1' value='Value 1'/>
                    <Picker.Item label='Value 2' value='Value 2'/>
                    <Picker.Item label='Value 3' value='Value 3'/>
                </Picker>
            );
        }
        return null;
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Text style={styles.fieldLabel}>Form Screen</Text>
                <View style={styles.fieldContainer}>
                    <Text style={styles.fieldLabel}>Text input</Text>
                    <TextInput
                        style={styles.textInputField}
                        onChangeText={(text) => this.setState({textInput: text})}
                        value={this.state.textInput}/>
                </View>
                <View style={styles.fieldContainer}>
                    <Text style={styles.fieldLabel}>Switch value</Text>
                    <Switch
                        onValueChange={(val) => this.setState({switchValue: val})}
                        value={this.state.switchValue}/>
                </View>
                <View style={styles.fieldContainer}>
                    <Text style={styles.fieldLabel}>Picker value</Text>
                    {this.createPicker()}
                </View>
                <View style={styles.buttonContainer}>
                    <Button
                        onPress={() => this.showValues()}
                        title="SHOW VALUES"/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    drawerOpenIcon: {
        marginLeft: 10
    },
    mainContainer: {
        flex: 1
    },
    fieldContainer: {
        flexDirection: 'row',
        alignItems: 'stretch',
        marginTop: 5
    },
    fieldLabel: {
        alignSelf: 'center',
        margin: 5
    },
    buttonContainer: {
        margin: 10
    },
    textInputField: {
        flex: 1,
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 5
    },
    pickerPlaceholder: {
        flex: 1,
        justifyContent: 'center',
        height: 40,
        margin: 5,
        borderColor: 'gray',
        borderWidth: 1
    },
    picker: {
        flex: 1,
        height: 40,
        margin: 5
    }
});

export default createStackNavigator({
    Home: {
        screen: FormScreen
    },
});
