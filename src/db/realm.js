import Realm from 'realm'

class Item {
    static get() {
        return realm.objects(Item.schema.name)
    }

    static schema = {
        name: 'Item',
        primaryKey: 'itemNumber',
        properties: {
            itemNumber: {type: 'string'},
            name: {type: 'string'},
            quantity: {type: 'int'},
            price: {type: 'float'},
            active: {type: 'bool', default: false}
        }
    }
}

export const insertItem = (itemNumber, name, quantity, price, active) => {
    realm.write(() => {
        realm.create(Item.schema.name, {
            itemNumber: itemNumber,
            name: name,
            quantity: quantity,
            price: price,
            active: active
        });
    });
}

export const insertItems = (items) => {
    realm.write(() => {
        for (item of items) {
            realm.create(Item.schema.name, {
                itemNumber: item.itemNumber,
                name: item.name,
                quantity: item.quantity,
                price: item.price,
                active: item.active
            });
        }
    });
}

export const countItems = () => {
    var items = realm.objects('Item');
    return items.length;
}

export const deleteAllItems = () => {
    realm.write(() => {
        let allItems = realm.objects('Item');
        realm.delete(allItems);
        console.log('Deleted ' + allItems.length + ' items');
    });
}

const realm = new Realm({schema: [Item]})
