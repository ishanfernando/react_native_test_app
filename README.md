# React Native Test App

This app tests various features of React Native, including:

1. AsyncStorage read/write
2. Realm storage read/write
3. Alert
4. ActivityIndicator
5. react-navigator and drawer
6. fetch data from network
7. write/read data from the file system
8. form with TextInput, Switch, Picker and ActionSheetIOS
9. styling
